////////////////////////////////////////////////////////////////////////////
//
//  Crytek Engine Source File.
//  Copyright (C), Crytek Studios, 2002.
// -------------------------------------------------------------------------
//  File name:   dllmain.cpp
//  Version:     v1.00
//  Created:     1/10/2002 by Timur.
//  Compilers:   Visual Studio.NET
//  Description: 
// -------------------------------------------------------------------------
//  History:
//
////////////////////////////////////////////////////////////////////////////

#include "StdAfx.h"
#include "DarkNetwork.h"
#include <platform_impl.h>

// For lua debugger
//#include <malloc.h>

HMODULE gDLLHandle = NULL;

#if !defined(_LIB) && !defined(LINUX) && !defined(PS3)
BOOL APIENTRY DllMain( HANDLE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
											)
{
	gDLLHandle = (HMODULE)hModule;
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_THREAD_ATTACH:    
	 
		
		break;
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH: 
		break;
	}

	return TRUE;
}
#endif

extern "C"
{
	DARKNETWORK_API IDarkNetwork* CreateDarkNetworkInterface(ISystem* pSystem)
	{
		CDarkNetwork *pDarkNetwork = NULL;

		if (!pSystem)
		{
			return 0;
		}
		pDarkNetwork = new CDarkNetwork(pSystem);

		if (!pDarkNetwork->Init())
		{
			delete pDarkNetwork;

			return 0;
		}

		return pDarkNetwork;
	}
};
