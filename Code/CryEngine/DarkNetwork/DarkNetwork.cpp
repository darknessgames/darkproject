#include "StdAfx.h"
#include "DarkNetwork.h"

// darkness::andriskaaz - Raknet Includes
#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>
#include <RakNetTypes.h>  // MessageID

#define SERVER_PORT 60000

enum GameMessages
{
	ID_GAME_MESSAGE_1 = ID_USER_PACKET_ENUM + 1
};

CDarkNetwork::CDarkNetwork(ISystem *pSystem)
:m_pSystem(NULL)
{
	m_pSystem = pSystem;
}

CDarkNetwork::~CDarkNetwork()
{
}

bool CDarkNetwork::Init()
{
	//gEnv->pLog->LogAlways("DarkNetwork Inited");
	m_pSystem->GetILog()->LogAlways("DarkNetwork Inited");

	RakNet::RakPeerInterface *peer = RakNet::RakPeerInterface::GetInstance();
	RakNet::Packet *packet;

	RakNet::SocketDescriptor sd;
	peer->Startup(1, &sd, 1);

	peer->Connect("localhost", SERVER_PORT, 0, 0);

	for (packet = peer->Receive(); packet; peer->DeallocatePacket(packet), packet = peer->Receive())
	{
		switch (packet->data[0])
		{
		case ID_CONNECTION_REQUEST_ACCEPTED:
		{
			m_pSystem->GetILog()->LogAlways("DarkNetwork Connection Accepted");
		}
			break;
		default:
			m_pSystem->GetILog()->LogAlways("Message with identifier %i has arrived.\n", packet->data[0]);
			break;
		}
	}

	return true;
}

void CDarkNetwork::Release()
{
}

bool CDarkNetwork::SendAndReceive(EDarkSendTo eSendTo)
{
	return false;
}

bool CDarkNetwork::SendPacket(EDarkSendTo eSendTo, EDarkPackets ePacketType, const PacketHelper* packetData, EDarkChannels eChannelID, int packetSize, DWORD peerID /* = 0 */)
{
	return false;
}