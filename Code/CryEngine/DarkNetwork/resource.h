//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CrySystem.rc
//
#define VS_VERSION_INFO                 1
#define IDD_CRITICAL_ERROR              101
#define IDD_EXCEPTION                   245
#define IDC_CALLSTACK                   1001
#define IDC_EXCEPTION_CODE              1002
#define IDC_EXCEPTION_ADDRESS           1003
#define IDC_EXCEPTION_MODULE            1004
#define IDC_EXCEPTION_DESC              1005
#define IDB_EXIT                        1008
#define IDB_IGNORE                      1010

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        127
#define _APS_NEXT_COMMAND_VALUE         40072
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
