#ifndef __CDARKNETWORK_H__
#define __CDARKNETWORK_H__

#if _MSC_VER > 1000
# pragma once
#endif


class CDarkNetwork : public IDarkNetwork
{
public:
	CDarkNetwork(ISystem *pSystem);
	~CDarkNetwork();

	// IDarkNetwork
	virtual bool Init();
	virtual void Release();
	virtual bool SendPacket(EDarkSendTo eSendTo, EDarkPackets ePacketType, const PacketHelper* packetData, EDarkChannels eChannelID, int packetSize, DWORD peerID = 0);
	virtual bool SendAndReceive(EDarkSendTo eSendTo);
	// ~IDarkNetwork
private:
	ISystem	*m_pSystem;
};

#endif// __CDARKNETWORK_H__