#if !defined(_DARKMASTER)
#include DEVIRTUALIZE_HEADER_FIX(IDarkNetwork.h)
#endif

#ifndef _DARK_NETWORK_H_
#define _DARK_NETWORK_H_

#if defined(_MSC_VER) 
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
#endif // defined(_MSC_VER)

#if !defined(_DARKMASTER)
#include <platform.h> // Needed for LARGE_INTEGER (for consoles).

#ifdef DARKNETWORK_EXPORTS
	#define DARKNETWORK_API DLL_EXPORT
#else
	#define DARKNETWORK_API DLL_IMPORT
#endif



#include "CryAssert.h"
#include "CompileTimeAssert.h"
#include "CryVersion.h"
#include "smartptr.h"
#else
#ifndef UNIQUE_IFACE
#define UNIQUE_IFACE
#endif // UNIQUE_IFACE
#endif // !defined_DARKMASTER

// Description:
//	DarkNetwork Packets.
enum EDarkPackets
{
	ePKT_Start = 135,
	ePKT_Test
};

// Description:
//	 DarkNetwork BasePacket.
#pragma pack(push)
#pragma pack(1)
struct PacketHelper
{
	const BYTE packetID;
	PacketHelper(int packet_ID) : packetID((BYTE)packet_ID) {}
};

template<int ePkt_ID>
struct BasePacket : public PacketHelper
{
	BasePacket() : PacketHelper(ePkt_ID) {}
};

struct testPacket : public BasePacket<ePKT_Start>
{
	const char* username;
};
#pragma pack(pop)

#if !defined(DWORD)
typedef unsigned long       DWORD;
#endif // DWORD

enum EDarkSendTo
{
	eSendTo_Client,
	eSendTo_Host,
	eSendTo_All,
	eSendTo_ThisPeer
};

enum EDarkChannels
{
	eChn_First,
	eChn_Last
};

// Description:
//	 DarkNetwork Interface.
//	 Initialize and dispatch IDarkNetwork.
UNIQUE_IFACE struct IDarkNetwork
{
	// Summary:
	//	 Init IDarkNetwork.
	virtual bool Init() = 0;

	// Summary:
	//	 Releases IDarkNetwork.
	virtual void Release() = 0;

	// Summary:
	//	 Send packet to the host or client.
	virtual bool SendPacket(EDarkSendTo eSendTo, EDarkPackets ePacketType, const PacketHelper* packetData, EDarkChannels eChannelID, int packetSize, DWORD peerID = 0) = 0;

	// Summary:
	//	 Send the packet and Receive the information.
	virtual bool SendAndReceive(EDarkSendTo eSendTo) = 0;
};

//////////////////////////////////////////////////////////////////////////////////////////
// DarkNetwork Macros.                                                
//////////////////////////////////////////////////////////////////////////////////////////
/*#define DEFINE_PACKET_FUNC(packetName, params) \
		void OnServerFunc_##packetName(Param_##funcName *params);*/
/*#define DEFINE_PACKET_HANDLER(packetName) \
		case packetName: \
		{ \
		Param_##packetName *paramCast = (Param_##packetName *)packetData; \
		break; \
		}

		struct Param_##packetName : public params{}; \
		*/

#define DEFINE_PACKET_HANDLER(packetName) \
	case(packetName): \
	{ \
		const Params_##packetName& paramCast = *(Params_##packetName*)packetData; \
		On_##packetName(paramCast); \
		\
	}

#define DEFINE_PACKET_FUNCTION(packetName, params) \
	public: \
	typedef params Params_##packetName; \
	bool On_##packetName(const Params_##packetName& param); 
	

#define IMPLEMENT_PACKET_HANDLER(className,packetName) \
	bool className::On_##packetName(const Params_##packetName& param)

//////////////////////////////////////////////////////////////////////////////////////////
// DarkNetwork Exports.                                                
//////////////////////////////////////////////////////////////////////////////////////////
#if !defined(_DARKMASTER)
typedef IDarkNetwork* (*PFNCREATEDARKNETWORKINTERFACE)(ISystem *pSystem);

extern "C"
{
	DARKNETWORK_API IDarkNetwork* CreateDarkNetworkInterface(ISystem *pSystem);
}
#endif
#endif //_DARK_NETWORK_H_

