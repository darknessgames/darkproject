#ifdef _DEBUG
	#define DEBUG_LOG_TO_CONSOLE 1
#else
	#define DEBUG_LOG_TO_CONSOLE 0
#endif // DEBUG

extern bool DarkLOG(const char* Str, ...);
extern bool InitDarkLOG();
