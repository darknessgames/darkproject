#include "stdafx.h"

#include "DarkHelper.h"

#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <windows.h>
#include <direct.h>

//darkness::amph - log directory + logprefix
static char szFileName[256] = "logs\\darklog_";

//darkness::amph - current format (yyyy-mm-dd_hh-mm-ss)
const std::string currentDateTime(bool isToFileName = false) {
	time_t     now = time(0);
	struct tm  *tstruct;
	char       buf[40];
	tstruct = localtime(&now);
	if (isToFileName)
		strftime(buf, sizeof(buf), "%Y_%m_%d_%I-%M-%S", tstruct);
	else
		strftime(buf, 40, "%c", localtime(&now));

	return buf;
}

bool InitDarkLOG()
{
	char temp[40];
	_mkdir("logs");
	sprintf_s(temp, "%s.log", currentDateTime(true));
	strcat(szFileName, temp);
	FILE * pFile;

	pFile = fopen(szFileName, "a+");
	if (!pFile)
	{
		printf("ERROR creating logfile at: %s\n", szFileName);
		return false;
	}
	else
	{
		fprintf(pFile, "DarkLOG Started on ");
		char tempdate[50];
		sprintf_s(tempdate, "%s\n", currentDateTime());
		fprintf(pFile, tempdate);

		fclose(pFile);
		printf("Log created -> %s\n", szFileName);
		return true;
	}
}

bool DarkLOG(const char* format, ...)
{
	FILE * pFile;

	pFile = fopen(szFileName, "a+");
	if (!pFile)
	{
		printf("ERROR opening logfile at: %s\n",szFileName);
		return false;
	}

	char temp[40];
	sprintf_s(temp, "%s: ", currentDateTime());
	fprintf(pFile, temp);

	char buffer[256];
	va_list args;
	va_start(args, format);
	vsnprintf(buffer, 256, format, args);
	va_end(args);

#if DEBUG_LOG_TO_CONSOLE 

	printf("DarknessDebugLog - %s\n", buffer);

#endif // darkness:amph - all log pass to console

	fprintf(pFile, buffer);

	char endslash[2];
	sprintf(endslash, "\n");
	fprintf(pFile, endslash);
	fflush(pFile);

	//fclose(pFile);

	return true;
}