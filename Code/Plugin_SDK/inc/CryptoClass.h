#pragma once

#include <CryptoPP/filters.h>
#include <CryptoPP/sha.h>
#include <CryptoPP/hex.h>

class CryptoClass
{
    public:
        static CryptoClass* getCryptoSingleton();

        bool checkHash( string pluginName, int pluginSize, const char* secretKey, string sHash );
        bool checkKey( const char* secretKey );

        // darkness::andriskaaz - Generated Plugin Stats
        int getPluginSize( const char* pluginPath );
        string getPluginHash( string pluginName );

    private:
        CryptoClass() {};
        static CryptoClass* m_pCrytoClass;

        string m_sHash;
        const char* storedSecretKey = "h+pHEb!asP#pAbAfU6Rer-X5Y?sT8*=25!uZAS$a6e+az+St$SpUq*42Ra42t5ph";
};