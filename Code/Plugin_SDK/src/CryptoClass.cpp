#include <StdAfx.h>
#include "CryptoClass.h"
#include <pluginStats_generated.tmp>
#include <sys/stat.h>

#ifndef SAFESTRING
#define SAFESTRING(x) (((const char*)x)?((const char*)x):"") //!< Helper to avoid NULL str problems
#endif

CryptoClass* CryptoClass::m_pCrytoClass = NULL;

CryptoClass* CryptoClass::getCryptoSingleton()
{
    if ( !m_pCrytoClass )
    {
        m_pCrytoClass = new CryptoClass;
    }

    return m_pCrytoClass;
}

bool CryptoClass::checkHash( string pluginName, int pluginSize, const char* secretKey, string sHash )
{
    CryptoPP::SHA256 sha1;
    std::string tempHash1, tempHash2;

    CryptoPP::StringSource( pluginName, true, new CryptoPP::HashFilter( sha1, new CryptoPP::HexEncoder( new CryptoPP::StringSink( tempHash1 ) ) ) );

    std::string pluginSizeTmpS = CryptoPP::IntToString( pluginSize );
    CryptoPP::StringSource( pluginSizeTmpS, true, new CryptoPP::HashFilter( sha1, new CryptoPP::HexEncoder( new CryptoPP::StringSink( tempHash2 ) ) ) );

    if ( strcmp( storedSecretKey, secretKey ) == 0 )
    {
        string sCryHashTemp1, sCryHashTemp2;

        sCryHashTemp1 = SAFESTRING( tempHash1.c_str() );
        sCryHashTemp2 = SAFESTRING( tempHash2.c_str() );

        m_sHash = sCryHashTemp1 + sCryHashTemp2;

        if ( m_sHash.compare( sHash ) == 0 )
        {
            return true;
        }

        else
        {
            return false;
        }

        return false;
    }

    else
    {
        return false;
    }

    return false;
}

bool CryptoClass::checkKey( const char* secretKey )
{
    if ( strcmp( storedSecretKey, secretKey ) == 0 )
    {
        return true;
    }

    return false;
}

int CryptoClass::getPluginSize( const char* pluginPath )
{
    struct stat stat_buf;
    int rc = stat( pluginPath, &stat_buf );
    return rc == 0 ? stat_buf.st_size : -1;
}

string CryptoClass::getPluginHash( string pluginName )
{
    if ( pluginName.compare( "Camera" ) == 0 )
    {
        return Camera_Hash;
    }

    else
    {
        return "";
    }
}