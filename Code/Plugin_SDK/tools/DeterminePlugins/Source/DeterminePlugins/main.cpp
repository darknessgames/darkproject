#include <iostream>
#include <fstream>
#include <windows.h>
#include <sys/stat.h>
#include <vector>
#include <ostream>
#include <regex>

#include <CryptoPP/filters.h>
#include <CryptoPP/sha.h>
#include <CryptoPP/hex.h>

std::string generateHash(std::string pluginName, int pluginSize)
{
	CryptoPP::SHA256 sha1;
	std::string tempHash1, tempHash2;

	CryptoPP::StringSource(pluginName, true, new CryptoPP::HashFilter(sha1, new CryptoPP::HexEncoder(new CryptoPP::StringSink(tempHash1))));

	std::string pluginSizeTmpS = CryptoPP::IntToString(pluginSize);
	CryptoPP::StringSource(pluginSizeTmpS, true, new CryptoPP::HashFilter(sha1, new CryptoPP::HexEncoder(new CryptoPP::StringSink(tempHash2))));

	return tempHash1 + tempHash2;
}

std::vector<std::string> getFilesOrFolder(std::string folder, bool isDirectory = false)
{
	std::vector<std::string> names;
	char search_path[200];
	sprintf_s(search_path, "%s*.*", folder.c_str());
	WIN32_FIND_DATA fd;
	HANDLE hFind = ::FindFirstFile(search_path, &fd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		if (isDirectory)
		{
			do
			{
				// read all (real) files in current folder, delete '!' read other 2 default folder . and ..
				if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
				{
					names.push_back(fd.cFileName);
				}
			} while (::FindNextFile(hFind, &fd));
			::FindClose(hFind);
		}
		else
		{
			do
			{
				// read all (real) files in current folder, delete '!' read other 2 default folder . and ..
				if (!(fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY))
				{
					names.push_back(fd.cFileName);
				}
			} while (::FindNextFile(hFind, &fd));
			::FindClose(hFind);
		}
	}
	return names;
}

int getSize(std::string filename)
{
	struct stat stat_buf;
	int rc = stat(filename.c_str(), &stat_buf);
	return rc == 0 ? stat_buf.st_size : -1;
}

int main(int argc, char* argv[])
{
	std::string dllDirectory = argv[1];

	/*HMODULE hModule = GetModuleHandleW(NULL);
	WCHAR path[MAX_PATH];
	GetModuleFileNameW(hModule, path, MAX_PATH);*/

	std::string pluginPath = "..\\..\\..\\" + dllDirectory +"\\Plugins\\";
	//std::string pluginPath = ".\\" + dllDirectory + "\\";
	std::vector<std::string> dirList = getFilesOrFolder(pluginPath, true);

	std::ofstream pluginStats;
	pluginStats.open("..\\inc\\pluginStats_generated.tmp");

	for (std::vector<std::string>::const_iterator dirIterator = dirList.begin(); dirIterator != dirList.end(); ++dirIterator)
	{
		//std::cout << dirIterator->c_str() << std::endl;

		std::string tempPath = pluginPath + dirIterator->c_str();
		tempPath += "\\";

		/*std::cout << "Plugin DIR: " << tempPath.c_str() << std::endl;
		std::cout << std::endl << std::endl;*/

		std::vector<std::string> pluginList = getFilesOrFolder(tempPath);

		for (std::vector<std::string>::const_iterator fileIterator = pluginList.begin(); fileIterator != pluginList.end(); ++fileIterator)
		{
			if (dirIterator->length() >= 3)
			{
				if (fileIterator->substr(fileIterator->find_last_of(".") + 1) == "dll")
				{
					std::string sHash = generateHash(dirIterator->c_str(), getSize(pluginPath + dirIterator->c_str() + "\\" + fileIterator->c_str()));
					pluginStats << "#define " << dirIterator->c_str() << "_Hash \"" << sHash << "\"\n";
					//pluginStats << "#define " << dirIterator->c_str() << " " << getSize(pluginPath + dirIterator->c_str() + "\\" + fileIterator->c_str()) << "\n";
					//std::cout << "Plugin Path: " << pluginPath + dirIterator->c_str() + "\\" + fileIterator->c_str() << std::endl;
				}
			}
		}
	}

	pluginStats.close();

	//system("PAUSE");
	return 0;
}