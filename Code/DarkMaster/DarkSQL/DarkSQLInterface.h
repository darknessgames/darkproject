#include <IDarkNetwork.h>
#include <RakString.h>

struct pg_conn;
typedef struct pg_conn PGconn;

struct pg_result;
typedef struct pg_result PGresult;

class CDarkSQLInterface
{
	public:
		CDarkSQLInterface();
		~CDarkSQLInterface();

		bool InitSQLConnection();
		bool Connect(const char *pghost, const char *pgport, const char *pgoptions,	const char *pgtty, const char *dbName, const char *login, const char *pwd);

		void CreateEmployeeTable(PGconn *conn);

	private:
		PGconn *pgConn;
		bool isConnected;
		const char *pghost;
		const char *pgport;
		const char *pgoptions;
		const char *pgtty;
		const char *dbName;
		const char *login;
		const char *pwd;

};