#include "stdafx.h"
#include "DarkSQLInterface.h"
#include "libpq-fe.h"

CDarkSQLInterface::CDarkSQLInterface()
{
	pgConn = 0;
	isConnected = false;

	pghost = "localhost";
	pgport = "5432";
	pgoptions = NULL; //darkness::amph - fogalmam sincs milyen option kell ide
	pgtty = NULL; //darkness:amph - connection timeout
	dbName = "postgres";
	login = "postgres";
	pwd = "jelszo1983";
}

CDarkSQLInterface::~CDarkSQLInterface()
{
	if (!pgConn)
		PQfinish(pgConn);
}

bool CDarkSQLInterface::InitSQLConnection()
{
	if (!Connect(pghost, pgport, NULL, NULL, dbName, login, pwd))
	{
		printf("DB Connection failed!\n");
		DarkLOG("MasterDB connection error!!!");
		return false;
	}
	else
	{
		printf("Connected to DarkMasterDB!\n");
		DarkLOG("Connected to DarkMasterDB!");
		return true;
	}
	return false;
}

bool CDarkSQLInterface::Connect(const char *pghost, const char *pgport, const char *pgoptions, const char *pgtty, const char *dbName, const char *login, const char *pwd)
{
		if (isConnected == false)
		{

			pgConn = PQsetdbLogin(pghost, pgport, pgoptions, pgtty, dbName, login, pwd);
			ConnStatusType status = PQstatus(pgConn);
			isConnected = status == CONNECTION_OK;
			if (isConnected == false)
			{
				PQfinish(pgConn);
				return false;
			}
		}
		//CreateEmployeeTable(pgConn);
		return isConnected;
}

void CDarkSQLInterface::CreateEmployeeTable(PGconn *conn)
{
	// Execute with sql statement
	PGresult *res = PQexec(conn, "CREATE TABLE employee (Fname char(30), Lname char(30))");

	if (PQresultStatus(res) != PGRES_COMMAND_OK)
	{
		printf("Create employee table failed");
		PQclear(res);
	}

	printf("Create employee table - OK\n");

	// Clear result
	PQclear(res);
}