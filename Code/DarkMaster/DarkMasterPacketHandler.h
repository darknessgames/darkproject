#pragma once

#include <IDarkNetwork.h>

class CDarkMasterPacketHandler : public IDarkPacketHandler
{
public:
	CDarkMasterPacketHandler();
	~CDarkMasterPacketHandler();

	// IDarkPacketHandler
	virtual void OnCallback(const PacketHelper* packetData, int len, EDarkChannels gotFrom);
	// ~IDarkPacketHandler

	void PrintThis(const char* msg);
	void SetIDarkNetwork(IDarkNetwork* pDarkNetwork) { m_pDarkNetwork = pDarkNetwork; }

private:
	IDarkNetwork* m_pDarkNetwork;
};

