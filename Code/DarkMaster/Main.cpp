#include "stdafx.h"
#include "DarkMaster.h"
#include "..\..\SDKs\DarkHelper\DarkHelper.h"

int main(int argc, char* argv[])
{
	//darkness::amph - log init
	extern bool InitDarkLOG();

	if (!InitDarkLOG())
		return false;

	CDarkMaster* pMaster = new CDarkMaster;

	if (pMaster->Init())
		DarkLOG("DarkMaster started");
	else return 0;

	DarkLOG("ASDASDASD");

	if (!pMaster->Run())
	{
		pMaster->Release();
		delete pMaster;
		pMaster = NULL;
	}

	return 0;
}