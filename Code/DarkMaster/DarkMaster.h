#include <IDarkNetwork.h>
#include <RakPeerInterface.h>
#include <MessageIdentifiers.h>
#include <BitStream.h>
#include <vector>
#include <memory>

// MasterServer Maximum User
#define MASTER_MAX_USER 10

class CDarkMaster : public IDarkNetwork
{
public:
	CDarkMaster();
	~CDarkMaster();

	// IDarkNetwork
	virtual bool Init();
	virtual void Release();
	virtual bool SendPacket(EDarkSendTo eSendTo, EDarkPackets ePacketType, const PacketHelper* packetData, EDarkChannels eChannelID, int packetSize, DWORD peerID = 0);
	virtual bool SendAndReceive(EDarkSendTo eSendTo);
	// ~IDarkNetwork

	bool Run();

private:
	RakNet::RakPeerInterface *masterPeer;
};

