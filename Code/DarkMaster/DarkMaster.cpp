#include "stdafx.h"
#include "DarkMaster.h"
#include "DarkSQL/DarkSQLInterface.h"

CDarkMaster::CDarkMaster()
{
}


CDarkMaster::~CDarkMaster()
{
}

bool CDarkMaster::Init()
{
	masterPeer = RakNet::RakPeerInterface::GetInstance();
	RakNet::SocketDescriptor sd(60000, 0);
	bool startResult = masterPeer->Startup(MASTER_MAX_USER, &sd, 1) == RakNet::RAKNET_STARTED;
	if (!startResult)
		return false;

	masterPeer->SetMaximumIncomingConnections(MASTER_MAX_USER);

	std::cout << "-------======= Master Server Initialized =======-------" << std::endl;

	CDarkSQLInterface * darkSQL = new CDarkSQLInterface;
	if (!darkSQL->InitSQLConnection())
	{
		printf("Not Connected to MasterDB, shutting down\n");
		return false;
	}


	return true;

}

bool CDarkMaster::Run()
{
	RakNet::Packet *packet = NULL;
	for (;;)
	{
		for (packet = masterPeer->Receive(); packet; masterPeer->DeallocatePacket(packet), packet = masterPeer->Receive())
		{
			switch (packet->data[0])
			{
			case ID_NEW_INCOMING_CONNECTION:
			{
				printf("New Incoming Connection From: %s\n", packet->guid.ToString());
			}
				break;
			
			case ID_DISCONNECTION_NOTIFICATION:
			{
				std::cout << "Peer Disconnected from: " << packet->systemAddress.ToString(true) << std::endl;
			}
				break;

			case ID_CONNECTION_LOST:
				printf("Connection lost");
				break;

			case ePKT_Start:
			{
				RakNet::RakString rs;
				RakNet::BitStream bsIn(packet->data, packet->length, false);
				bsIn.IgnoreBytes(sizeof(RakNet::MessageID));
				bsIn.Read(rs);

				std::cout << rs.C_String() << std::endl;

				RakNet::BitStream bsOut;
				bsOut.Write((RakNet::MessageID)ePKT_Start);
				bsOut.Write("Mastertol");

				masterPeer->Send(&bsOut, HIGH_PRIORITY, RELIABLE, 0, packet->systemAddress, false);
			}
				break;

			default:
				break;
			}
		}
	}

	return false;
}

void CDarkMaster::Release()
{
	masterPeer->Shutdown(300);
	RakNet::RakPeerInterface::DestroyInstance(masterPeer);
}

bool CDarkMaster::SendPacket(EDarkSendTo eSendTo, EDarkPackets ePacketType, const PacketHelper* packetData, EDarkChannels eChannelID, int packetSize, DWORD peerID)
{
	if (eChannelID == NULL)
		return false;
	
	switch (eSendTo)
	{
	case eSendTo_Client:
		break;
	case eSendTo_Host:
		break;
	case eSendTo_All:
		break;
	case eSendTo_ThisPeer:
		break;
	default:
		break;
	}

	return false;
}

bool CDarkMaster::SendAndReceive(EDarkSendTo eSendTo)
{
	return false;
}